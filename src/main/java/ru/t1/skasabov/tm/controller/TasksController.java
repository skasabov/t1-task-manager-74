package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.skasabov.tm.dto.CustomUser;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

import java.util.Collections;
import java.util.List;

@Controller
public final class TasksController {

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView index(@AuthenticationPrincipal @NotNull final CustomUser user) {
        @Nullable List<TaskDto> tasks = taskRepository.findAllByUserId(user.getUserId());
        if (tasks == null) tasks = Collections.emptyList();
        @NotNull final ModelAndView modelAndView = new ModelAndView("task-list", "tasks", tasks);
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

}
