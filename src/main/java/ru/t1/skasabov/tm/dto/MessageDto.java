package ru.t1.skasabov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class MessageDto {

    @NotNull
    private String value;

    public MessageDto(@NotNull final String value) {
        this.value = value;
    }

}
