package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.IAuthEndpoint;
import ru.t1.skasabov.tm.dto.Result;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.repository.UserDtoRepository;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.t1.skasabov.tm.api.IAuthEndpoint")
public final class AuthEndpointImpl implements IAuthEndpoint {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private UserDtoRepository userRepository;

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/login")
    public Result login(
            @NotNull
            @RequestParam("login")
            @WebParam(name = "login", partName = "login")
            final String username,
            @NotNull
            @RequestParam("password")
            @WebParam(name = "password", partName = "password")
            final String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        }
        catch(@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/profile")
    public UserDto profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String username = authentication.getName();
        return userRepository.findByLogin(username);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
