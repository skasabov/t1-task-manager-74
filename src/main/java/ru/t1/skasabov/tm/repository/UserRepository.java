package ru.t1.skasabov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.skasabov.tm.model.User;

public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull String login);

}
