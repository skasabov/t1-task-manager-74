package ru.t1.skasabov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.skasabov.tm.dto.CustomUser;

public interface UserUtil {

    @NotNull
    static String getUserId() {
        @NotNull final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        @Nullable final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("");
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("");
        @NotNull final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}
