package ru.t1.skasabov.tm.integration.rest;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.client.rest.AuthRestEndpointClient;
import ru.t1.skasabov.tm.client.rest.TaskRestEndpointClient;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskRestEndpointClientTest {

    private static final int NUMBER_OF_ENTITIES = 4;

    @NotNull
    private final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    private final AuthRestEndpointClient authClient = AuthRestEndpointClient.client();

    @NotNull
    private final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    private final TaskDto task2 = new TaskDto("Test Task 2");

    @NotNull
    private final TaskDto task3 = new TaskDto("Test Task 3");

    @NotNull
    private final TaskDto task4 = new TaskDto("Test Task 4");

    private boolean isAuth = false;

    @NotNull
    private String userId;

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    @Before
    public void initTest() {
        Assert.assertTrue(authClient.login("test", "test").isSuccess());
        @Nullable final UserDto user = authClient.profile();
        Assert.assertNotNull(user);
        userId = user.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        task4.setUserId(userId);
        @Nullable final List<TaskDto> tasks = client.findAll();
        taskList = tasks == null ? Collections.emptyList() : tasks;
        client.clear();
        client.save(task1);
        client.save(task2);
        client.save(task3);
        client.save(task4);
    }

    @After
    public void clean() {
        if (isAuth) {
            Assert.assertTrue(authClient.login("test", "test").isSuccess());
        }
        isAuth = false;
        client.clear();
        for (@NotNull final TaskDto task : taskList) client.save(task);
        authClient.logout();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllTest() {
        @Nullable final List<TaskDto> tasks = client.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(NUMBER_OF_ENTITIES, tasks.size());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void findAllNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.findAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveTest() {
        @NotNull final TaskDto task = new TaskDto("Test Task");
        task.setUserId(userId);
        client.save(task);
        Assert.assertEquals(NUMBER_OF_ENTITIES + 1, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void saveNoAuthTest() {
        isAuth = true;
        authClient.logout();
        @NotNull final TaskDto task = new TaskDto("Test Task");
        task.setUserId(userId);
        client.save(task);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByIdTest() {
        @Nullable final TaskDto Task = client.findById(task1.getId());
        Assert.assertNotNull(Task);
        Assert.assertEquals("Test Task 1", Task.getName());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void findByEmptyIdTest() {
        client.findById("");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByInvalidIdTest() {
        Assert.assertNull(client.findById("123"));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void findByIdNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.findById(task1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByIdTest() {
        Assert.assertTrue(client.existsById(task1.getId()));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void existsByEmptyIdTest() {
        client.existsById("");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByInvalidIdTest() {
        Assert.assertFalse(client.existsById("123"));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void existsByIdNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.existsById(task1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void countTest() {
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void countNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.count();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByIdTest() {
        client.deleteById(task1.getId());
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void deleteByEmptyIdTest() {
        client.deleteById("");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByInvalidIdTest() {
        client.deleteById("some_id");
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void deleteByIdNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.deleteById(task1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteTest() {
        client.delete(task1);
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void deleteNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.delete(task1);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteTasksTest() {
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task2);
        tasks.add(task3);
        tasks.add(task4);
        client.deleteAll(tasks);
        Assert.assertEquals(NUMBER_OF_ENTITIES - tasks.size(), client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteEmptyTasksTest() {
        client.deleteAll(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullTasksTest() {
        client.deleteAll(null);
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void deleteProjectsNoAuthTest() {
        isAuth = true;
        authClient.logout();
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task2);
        tasks.add(task3);
        tasks.add(task4);
        client.deleteAll(tasks);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void clearTest() {
        client.clear();
        Assert.assertEquals(0, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void clearNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.clear();
    }
    
}
