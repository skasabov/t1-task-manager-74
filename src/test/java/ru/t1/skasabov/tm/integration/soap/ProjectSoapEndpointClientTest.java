package ru.t1.skasabov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.t1.skasabov.tm.api.IAuthEndpoint;
import ru.t1.skasabov.tm.api.IProjectEndpoint;
import ru.t1.skasabov.tm.api.ITaskEndpoint;
import ru.t1.skasabov.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.skasabov.tm.client.soap.ProjectSoapEndpointClient;
import ru.t1.skasabov.tm.client.soap.TaskSoapEndpointClient;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.endpoint.AuthEndpointImpl;
import ru.t1.skasabov.tm.endpoint.ProjectEndpointImpl;
import ru.t1.skasabov.tm.endpoint.TaskEndpointImpl;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectSoapEndpointClientTest {

    private static final int NUMBER_OF_ENTITIES = 4;

    @NotNull
    private static final String BASE_URL = "http://localhost:8080";

    @NotNull
    private static IProjectEndpoint PROJECT_ENDPOINT = new ProjectEndpointImpl();

    @NotNull
    private static ITaskEndpoint TASK_ENDPOINT = new TaskEndpointImpl();

    @NotNull
    private static IAuthEndpoint AUTH_ENDPOINT = new AuthEndpointImpl();

    @NotNull
    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    @NotNull
    private final ProjectDto project3 = new ProjectDto("Test Project 3");

    @NotNull
    private final ProjectDto project4 = new ProjectDto("Test Project 4");

    private boolean isAuth = false;

    @NotNull
    private String userId;

    @NotNull
    private List<ProjectDto> projectList = new ArrayList<>();

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    @BeforeClass
    @SneakyThrows
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static void initEndpoint() {
        AUTH_ENDPOINT = AuthSoapEndpointClient.getInstance(BASE_URL);
        Assert.assertTrue(AUTH_ENDPOINT.login("test", "test").isSuccess());
        PROJECT_ENDPOINT = ProjectSoapEndpointClient.getInstance(BASE_URL);
        TASK_ENDPOINT = TaskSoapEndpointClient.getInstance(BASE_URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) AUTH_ENDPOINT;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) PROJECT_ENDPOINT;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) TASK_ENDPOINT;
        @Nullable Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        AUTH_ENDPOINT.logout();
    }

    @Before
    public void initTest() {
        @Nullable final UserDto user = AUTH_ENDPOINT.profile();
        Assert.assertNotNull(user);
        userId = user.getId();
        @Nullable final List<ProjectDto> projects = PROJECT_ENDPOINT.findAll();
        projectList = projects == null ? Collections.emptyList() : projects;
        @Nullable final List<TaskDto> tasks = TASK_ENDPOINT.findAll();
        taskList = tasks == null ? Collections.emptyList() : tasks;
        PROJECT_ENDPOINT.clear();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        project4.setUserId(userId);
        PROJECT_ENDPOINT.save(project1);
        PROJECT_ENDPOINT.save(project2);
        PROJECT_ENDPOINT.save(project3);
        PROJECT_ENDPOINT.save(project4);
        @NotNull final TaskDto task1 = new TaskDto("Test Task 1");
        task1.setProjectId(project1.getId());
        task1.setUserId(userId);
        @NotNull final TaskDto task2 = new TaskDto("Test Task 2");
        task2.setProjectId(project2.getId());
        task2.setUserId(userId);
        @NotNull final TaskDto task3 = new TaskDto("Test Task 3");
        task3.setProjectId(project3.getId());
        task3.setUserId(userId);
        @NotNull final TaskDto task4 = new TaskDto("Test Task 4");
        task4.setProjectId(project4.getId());
        task4.setUserId(userId);
        TASK_ENDPOINT.save(task1);
        TASK_ENDPOINT.save(task2);
        TASK_ENDPOINT.save(task3);
        TASK_ENDPOINT.save(task4);
    }

    @After
    public void clean() {
        if (isAuth) {
            Assert.assertTrue(AUTH_ENDPOINT.login("test", "test").isSuccess());
        }
        isAuth = false;
        PROJECT_ENDPOINT.clear();
        for (@NotNull final TaskDto task : taskList) TASK_ENDPOINT.save(task);
        for (@NotNull final ProjectDto project : projectList) PROJECT_ENDPOINT.save(project);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllTest() {
        @Nullable final List<ProjectDto> projects = PROJECT_ENDPOINT.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(NUMBER_OF_ENTITIES, projects.size());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void findAllNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        PROJECT_ENDPOINT.findAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveTest() {
        @NotNull final ProjectDto project = new ProjectDto("Test Project");
        project.setUserId(userId);
        PROJECT_ENDPOINT.save(project);
        Assert.assertEquals(NUMBER_OF_ENTITIES + 1, PROJECT_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void saveNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        @NotNull final ProjectDto project = new ProjectDto("Test Project");
        project.setUserId(userId);
        PROJECT_ENDPOINT.save(project);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByIdTest() {
        @Nullable final ProjectDto project = PROJECT_ENDPOINT.findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals("Test Project 1", project.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByEmptyIdTest() {
        Assert.assertNull(PROJECT_ENDPOINT.findById(""));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByInvalidIdTest() {
        Assert.assertNull(PROJECT_ENDPOINT.findById("123"));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void findByIdNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        PROJECT_ENDPOINT.findById(project1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByIdTest() {
        Assert.assertTrue(PROJECT_ENDPOINT.existsById(project1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByEmptyIdTest() {
        Assert.assertFalse(PROJECT_ENDPOINT.existsById(""));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByInvalidIdTest() {
        Assert.assertFalse(PROJECT_ENDPOINT.existsById("123"));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void existsByIdNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        PROJECT_ENDPOINT.existsById(project1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void countTest() {
        Assert.assertEquals(NUMBER_OF_ENTITIES, PROJECT_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void countNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        PROJECT_ENDPOINT.count();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByIdTest() {
        PROJECT_ENDPOINT.deleteById(project1.getId());
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, PROJECT_ENDPOINT.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByEmptyIdTest() {
        PROJECT_ENDPOINT.deleteById("");
        Assert.assertEquals(NUMBER_OF_ENTITIES, PROJECT_ENDPOINT.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByInvalidIdTest() {
        PROJECT_ENDPOINT.deleteById("some_id");
        Assert.assertEquals(NUMBER_OF_ENTITIES, PROJECT_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void deleteByIdNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        PROJECT_ENDPOINT.deleteById(project1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteTest() {
        PROJECT_ENDPOINT.delete(project1);
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, PROJECT_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void deleteNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        PROJECT_ENDPOINT.delete(project1);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteProjectsTest() {
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(project2);
        projects.add(project3);
        projects.add(project4);
        PROJECT_ENDPOINT.deleteAll(projects);
        Assert.assertEquals(NUMBER_OF_ENTITIES - projects.size(), PROJECT_ENDPOINT.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteEmptyProjectsTest() {
        PROJECT_ENDPOINT.deleteAll(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTITIES, PROJECT_ENDPOINT.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteNullProjectsTest() {
        PROJECT_ENDPOINT.deleteAll(null);
        Assert.assertEquals(NUMBER_OF_ENTITIES, PROJECT_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void deleteProjectsNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(project2);
        projects.add(project3);
        projects.add(project4);
        PROJECT_ENDPOINT.deleteAll(projects);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void clearTest() {
        PROJECT_ENDPOINT.clear();
        Assert.assertEquals(0, PROJECT_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void clearNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        PROJECT_ENDPOINT.clear();
    }

}
