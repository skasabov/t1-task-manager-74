package ru.t1.skasabov.tm.integration.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.api.IAuthEndpoint;
import ru.t1.skasabov.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.endpoint.AuthEndpointImpl;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

public class AuthSoapEndpointClientTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint AUTH_ENDPOINT = new AuthEndpointImpl();

    private boolean isLogout = true;

    @BeforeClass
    @SneakyThrows
    public static void initEndpoint() {
        AUTH_ENDPOINT = AuthSoapEndpointClient.getInstance(BASE_URL);
    }

    @Before
    public void initTest() {
        Assert.assertTrue(AUTH_ENDPOINT.login("test", "test").isSuccess());
    }

    @After
    public void clean() {
        if (isLogout) {
            AUTH_ENDPOINT.logout();
        }
        isLogout = true;
    }

    @Test
    @Category(IntegrationCategory.class)
    public void loginTest() {
        AUTH_ENDPOINT.logout();
        Assert.assertTrue(AUTH_ENDPOINT.login("test", "test").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void emptyLoginTest() {
        isLogout = false;
        AUTH_ENDPOINT.logout();
        Assert.assertFalse(AUTH_ENDPOINT.login("", "test").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void loginEmptyPasswordTest() {
        isLogout = false;
        AUTH_ENDPOINT.logout();
        Assert.assertFalse(AUTH_ENDPOINT.login("test", "").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void incorrectLoginTest() {
        isLogout = false;
        AUTH_ENDPOINT.logout();
        Assert.assertFalse(AUTH_ENDPOINT.login("admin", "test").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void loginIncorrectPasswordTest() {
        isLogout = false;
        AUTH_ENDPOINT.logout();
        Assert.assertFalse(AUTH_ENDPOINT.login("test", "admin").isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void profileTest() {
        @Nullable final UserDto user = AUTH_ENDPOINT.profile();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void profileNoAuthTest() {
        isLogout = false;
        AUTH_ENDPOINT.logout();
        Assert.assertNull(AUTH_ENDPOINT.profile());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void logoutTest() {
        isLogout = false;
        Assert.assertFalse(AUTH_ENDPOINT.logout().isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void logoutNoAuthTest() {
        isLogout = false;
        AUTH_ENDPOINT.logout();
        Assert.assertFalse(AUTH_ENDPOINT.logout().isSuccess());
    }

}
